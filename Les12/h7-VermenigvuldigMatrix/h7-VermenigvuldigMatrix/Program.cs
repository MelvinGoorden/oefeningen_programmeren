﻿using System;

namespace h7_VermenigvuldigMatrix
{
    class Program
    {
        static void Main(string[] args)
        {
            double[,] Matrix1 ={
                  {2,4},
                  {3,5}
                };
            double[,] Matrix2 ={
                  {2,4},
                  {3,5}
                };

            double[,] answers = MatrixMultiplier(Matrix1, Matrix2);
            VisualiseArray(answers);
        }

        private static double[,] MatrixMultiplier(double[,] matrix1, double[,] matrix2)
        {
            double[,] answer = new double[matrix1.GetLength(0), matrix1.GetLength(1)];
            for (int i = 0; i < matrix1.GetLength(0); i++)
            {
                for (int y = 0; y < matrix1.GetLength(1); y++)
                {
                    answer[i, y] = matrix1[i, y] * matrix2[i, y];
                }
            }
            return answer;
        }
        private static void VisualiseArray(double[,] array)
        {
            for (int i = 0; i < array.GetLength(0); i++)
            {
                for (int y = 0; y < array.GetLength(1); y++)
                {
                    Console.Write($"{array[i, y]}\t");
                }
                Console.WriteLine();
            }

        }
    }
}
