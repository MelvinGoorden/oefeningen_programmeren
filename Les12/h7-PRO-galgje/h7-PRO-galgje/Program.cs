﻿using System;
using System.Linq;

namespace h7_PRO_galgje
{
    class Program
    {
        static void Main(string[] args)
        {
            int attempts = 0;
            Console.Write("Geef een woord in: ");
            char[] answer = Console.ReadLine().ToCharArray();
            Console.Clear();
            char[] userAttempt = new char[answer.Length];
            for (int i = 0; i < answer.Length; i++)
            {
                userAttempt[i] = '*';
            }

            while(!(Enumerable.SequenceEqual(answer, userAttempt)))
            {
                for (int i = 0; i < answer.Length; i++)
                {
                    Console.Write(userAttempt[i]);

                }
                Console.WriteLine();
                Console.WriteLine("Geef één letter in, of raad het volledige woord. ");
                char[] input = Console.ReadLine().ToCharArray();

                if (input.Length == 1)
                {
                    CheckLetter(input[0], answer, userAttempt);
                }
                else
                {
                    CheckWord(input, answer, userAttempt);
                }
                attempts++;
            }

            Console.WriteLine("Correct!! U heeft het juiste woord geraden");
            Console.WriteLine($"Benodigde pogingen {attempts}");
        }

        private static void CheckWord(char[] input, char[] answer, char[] userAttempt)
        {
            int counter = 0;
            if (answer.Length == input.Length)
            {
                for (int i = 0; i < answer.Length; i++)
                {
                    if (answer[i] == input[i])
                    {
                        counter++;
                    }
                }
                if(counter == answer.Length)
                {
                    for (int i = 0; i < answer.Length; i++)
                    {
                        userAttempt[i] = answer[i];
                    }
                }
            }    
        }

        private static void CheckLetter(char input, char[] answer, char[] userAttempt)
        {
            for (int i = 0; i < answer.Length; i++)
            {
                if (input == answer[i])
                {
                    userAttempt[i] = input;
                }
            }
        }
    }
}
