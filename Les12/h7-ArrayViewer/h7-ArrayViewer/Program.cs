﻿using System;

namespace Programmeren
{
    class Program
    {
        static void Main(string[] args)
        {
            int[,] array = { { 15, 6, 9 }, { 1, 2, 3 }, { 6, 9, 12 } };
            VisualiseArray(array);
        }

        private static void VisualiseArray(int[,] array)
        {
            for (int i = 0; i < array.GetLength(0); i++)
            {
                for (int y = 0; y < array.GetLength(1); y++)
                {
                    Console.Write($"{array[i,y]}\t");
                }
                Console.WriteLine();
            }
            
        }
    }
}