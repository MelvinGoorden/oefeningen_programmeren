﻿using System;

namespace Programmeren
{
    class Program
    {
        static void Main(string[] args)
        {

            int[,] aMatrix ={
                  {2,4},
                  {3,5}
                };
            Console.WriteLine($"Determinant van matrix is {CalculateDeterminant(aMatrix)}");
        }

        private static int CalculateDeterminant(int[,] matrix)
        {
            int determinant = 0;

            determinant = (matrix[0,0] * matrix[1, 1]) - (matrix[0, 1] * matrix[1, 0]);
            
            return determinant;
        }
    }
}
