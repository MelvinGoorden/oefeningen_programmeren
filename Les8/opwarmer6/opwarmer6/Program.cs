﻿using System;

namespace opwarmer6
{
    class Program
    {
        static void Main(string[] args)
        {
            for(int teller = 0; teller < 101; teller++)
            {
                if ((teller % 6 == 0) && (teller % 8 == 0))
                {
                    Console.WriteLine(teller);
                }
            }
        }
    }
}
