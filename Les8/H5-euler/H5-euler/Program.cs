﻿using System;

namespace H5_euler
{
    class Program
    {
        static void Main(string[] args)
        {
            int getal = 0;
            for(int i = 0; i <= 1000; i++)
            {
                if((i % 3 == 0) || (i % 5 == 0))
                {
                    getal += i;
                }
            }

            Console.WriteLine(getal);
        }
    }
}
