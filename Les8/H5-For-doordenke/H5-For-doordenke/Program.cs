﻿using System;

namespace H5_For_doordenke
{
    class Program
    {
        static void Main(string[] args)
        {

            int max, teller = 0;

            Console.Write("geef het maximum in: ");
            max = Convert.ToInt32(Console.ReadLine());

            
            for(int x = 0; x < max; x++)
            {
                for (int y = 0; y < teller; y++)
                {
                    Console.Write("*");
                }
                teller++;
                Console.WriteLine();
            }

            for (int x = 0; x < max; x++)
            {
                for (int y = 0; y < teller; y++)
                {
                    Console.Write("*");
                }
                teller--;
                Console.WriteLine();
            }



        }
    }
}
