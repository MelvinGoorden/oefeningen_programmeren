﻿using System;

namespace RNA_Transscriptie
{
    class Program
    {
        static void Main(string[] args)
        {
            string rna = null;
            string dna;
            int teller = 0;

            Console.WriteLine("geef een dna nucleotide in.");

            while(teller <= 10)
            {
                dna = Console.ReadLine();

                if(dna == "G")
                {
                    rna += "C";
                }
                else if(dna == "C")
                {
                    rna += "G";
                }
                else if (dna == "T")
                {
                    rna += "A";
                }
                else if (dna == "A")
                {
                    rna += "U";
                }
                teller++;
            }
            Console.WriteLine(rna);
        }
    }
}
