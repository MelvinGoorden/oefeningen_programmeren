﻿using System;

namespace H5_Armstrong_nummer
{
    class Program
    {
        static void Main(string[] args)
        {
            int length, teller = 0;
            double getal, output = 0;
            string input;

            Console.Write("geef een getal in: ");
            input = Console.ReadLine();

            length = input.Length;
            getal = Convert.ToInt32(input);

            for(int i = 1; i <= length; i++)
            {
                while (getal >= Math.Pow(10, length - i))
                {
                    getal -= Math.Pow(10, length - i);
                    teller++;
                }
                output += Math.Pow(teller, length);
                teller = 0;
            }

            if(output.ToString() == input)
            {
                Console.WriteLine("het getal is een getal van armstrong!!");
            }
            else
            {
                Console.WriteLine("geen getal van armstrong");
            }
        }
    }
}
