﻿using System;

namespace H3_op_de_poef
{
    class Program
    {
        static void Main(string[] args)
        {
            int bedrag = 0;
            int betalingen = 0;

            for (int i = 0; i < 5; i++)
            {
                Console.WriteLine("Voer bedrag in?");
                bedrag += Convert.ToInt32(Console.ReadLine());
                Console.WriteLine($"De poef staat op {bedrag} euro");
            }

            Console.WriteLine("*************************");
            Console.WriteLine($"Het totaal van  de poef is {bedrag} euro.");
            while(bedrag > 0)
            {
                betalingen++;
                bedrag -= 10;

            }
            Console.WriteLine($"Dit zal {betalingen} afbetalingen vragen.");
        }
    }
}
