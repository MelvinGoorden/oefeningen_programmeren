﻿using System;

namespace H3_geometric_fun
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Geef een hoek, uitgedrukt in graden.");
            int graden = Convert.ToInt32(Console.ReadLine());

            double radialen = graden * (Math.PI / 180);
            Console.WriteLine($"{graden} graden is {Math.Round(radialen,2)} radialen.");
            Console.WriteLine($"De sinus is {Math.Round(Math.Sin(radialen),2)}.");
            Console.WriteLine($"De cosinus is {Math.Round(Math.Cos(radialen), 2)}.");
            Console.WriteLine($"De tangens is {Math.Round(Math.Tan(radialen), 2)}.");
        }
    }
}
