﻿using System;

namespace H3_feestkassa
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Frietjes?");
            int friet = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Koninginnenhapjes?");
            int koninginnenhapje = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Ijsjes?");
            int ijsje = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Dranken?");
            int drank = Convert.ToInt32(Console.ReadLine());

            int bedrag = (friet * 20) + (koninginnenhapje * 10) + (ijsje * 3) + (drank * 2);

            Console.WriteLine($"Het totaal te betalen bedrag is {bedrag} EURO");
        }
    }
}
