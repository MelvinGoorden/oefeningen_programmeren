﻿using System;

namespace H3_random_invoer
{
    class Program
    {
        static void Main(string[] args)
        {
            int bedrag = 0;
            int betalingen = 0;
            Random randomgen = new Random();

            for (int i = 0; i < 5; i++)
            {
                Console.WriteLine("Voer bedrag in?");
                int getal = randomgen.Next(0, 51);
                bedrag += getal;
                Console.WriteLine($"Automatisch gegenereerd: {getal}");
                Console.WriteLine($"De poef staat op {bedrag} euro");
            }

            Console.WriteLine("*************************");
            Console.WriteLine($"Het totaal van  de poef is {bedrag} euro.");
            while (bedrag > 0)
            {
                betalingen++;
                bedrag -= 10;

            }
            Console.WriteLine($"Dit zal {betalingen} afbetalingen vragen.");
        }
    }
}
