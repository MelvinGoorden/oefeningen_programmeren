﻿using System;

namespace H3_binaire_god
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Geef een getal.");
            int getal = Convert.ToInt32(Console.ReadLine());
            getal = getal >> 1;
            getal = getal << 1;

            Console.WriteLine($"Het getal is nu {getal}.");
            getal = getal << 3;
            Console.WriteLine($"Het getal is nu {getal}.");

            Console.WriteLine("Geef een binaire string.");
            int binair = Convert.ToInt32(Console.ReadLine(),2);

            binair = binair >> 1;
            binair = binair << 1;
            Console.WriteLine($"Het getal is nu {Convert.ToString(binair, 2)}.");
            binair = binair << 4;
            Console.WriteLine($"Het getal is nu {Convert.ToString(binair, 2)}.");
        }
    }
}
