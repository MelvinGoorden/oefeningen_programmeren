﻿using System;

namespace H3_BMI_berekenaar
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hoe veel weeg je in kg?");
            double gewicht = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Hoe groot ben je in m?");
            double grootte = Convert.ToDouble(Console.ReadLine());

            double BMI = gewicht / Math.Pow(grootte, 2);
            Console.WriteLine($"Je BMI bedraagt {Math.Round(BMI, 2)}.");
        }
    }
}
