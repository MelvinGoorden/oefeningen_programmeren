﻿using System;

namespace H4_ruimte_specifiek
{
    class Program
    {
        enum planeten {Mercurius = 1, Venus, Aarde, Mars, Jupiter, Saturnus, Uranus, Neptunus, Pluto}
        static void Main(string[] args)
        {
            int massa, keuze;
            double gewicht;
            planeten planeet;


            Console.WriteLine("Hoeveel weeg je?");
            massa = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Voor welke planeet wil je je gewicht kennen ? (nummer)");
            Console.WriteLine("1. Mercurius");
            Console.WriteLine("2. Venus");
            Console.WriteLine("3. Aarde");
            Console.WriteLine("4. Mars");
            Console.WriteLine("5. Jupiter");
            Console.WriteLine("6. Saturnus");
            Console.WriteLine("7. Uranus");
            Console.WriteLine("8. Neptunus");
            Console.WriteLine("9. Pluto");

            keuze = Convert.ToInt32(Console.ReadLine());
            planeet = (planeten)keuze;

            switch (planeet)
            {
                case planeten.Mercurius:
                    gewicht = massa * 0.38;
                    Console.WriteLine($"Op Mercurius heb je een schijnbaar gewicht van {gewicht}kg.");
                    break;
                case planeten.Venus:
                    gewicht = massa * 0.91;
                    Console.WriteLine($"Op Venus heb je een schijnbaar gewicht van {gewicht}kg.");
                    break;
                case planeten.Aarde:
                    gewicht = massa * 1.00;
                    Console.WriteLine($"Op Aarde heb je een schijnbaar gewicht van {gewicht}kg.");
                    break;
                case planeten.Mars:
                    gewicht = massa * 0.38;
                    Console.WriteLine($"Op Mars heb je een schijnbaar gewicht van {gewicht}kg.");
                    break;
                case planeten.Jupiter:
                    gewicht = massa * 2.34;
                    Console.WriteLine($"Op Jupiter heb je een schijnbaar gewicht van {gewicht}kg.");
                    break;
                case planeten.Saturnus:
                    gewicht = massa * 1.06;
                    Console.WriteLine($"Op Saturnus heb je een schijnbaar gewicht van {gewicht}kg.");
                    break;
                case planeten.Uranus:
                    gewicht = massa * 0.92;
                    Console.WriteLine($"Op Uranus heb je een schijnbaar gewicht van {gewicht}kg.");
                    break;
                case planeten.Neptunus:
                    gewicht = massa * 1.19;
                    Console.WriteLine($"Op Neptunus heb je een schijnbaar gewicht van {gewicht}kg.");
                    break;
                case planeten.Pluto:
                    gewicht = massa * 0.06;
                    Console.WriteLine($"Op Pluto heb je een schijnbaar gewicht van {gewicht}kg.");
                    break;
                default:
                    Console.WriteLine("keuze niet mogelijk probeer opnieuw.");
                    break;
            }
        }
    }
}
