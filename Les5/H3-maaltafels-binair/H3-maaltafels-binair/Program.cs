﻿using System;

namespace H3_maaltafels_binair
{
    class Program
    {
        static void Main(string[] args)
        {
            Random randomgen = new Random();
            int getal, binair, uitkomst;

            getal = randomgen.Next(0, 11);
            binair = Convert.ToInt32("1", 2);
            uitkomst = getal * binair;

            Console.WriteLine($"{Convert.ToString(binair, 2)} * {Convert.ToString(getal, 2)} = {Convert.ToString(uitkomst, 2)}");

            getal = randomgen.Next(0, 11);
            binair = Convert.ToInt32("10", 2);
            uitkomst = getal * binair;

            Console.WriteLine($"{Convert.ToString(binair, 2)} * {Convert.ToString(getal, 2)} = {Convert.ToString(uitkomst, 2)}");

            getal = randomgen.Next(0, 11);
            binair = Convert.ToInt32("11", 2);
            uitkomst = getal * binair;

            Console.WriteLine($"{Convert.ToString(binair, 2)} * {Convert.ToString(getal, 2)} = {Convert.ToString(uitkomst, 2)}");

            getal = randomgen.Next(0, 11);
            binair = Convert.ToInt32("100", 2);
            uitkomst = getal * binair;

            Console.WriteLine($"{Convert.ToString(binair, 2)} * {Convert.ToString(getal, 2)} = {Convert.ToString(uitkomst, 2)}");

            getal = randomgen.Next(0, 11);
            binair = Convert.ToInt32("101", 2);
            uitkomst = getal * binair;

            Console.WriteLine($"{Convert.ToString(binair, 2)} * {Convert.ToString(getal, 2)} = {Convert.ToString(uitkomst, 2)}");

            getal = randomgen.Next(0, 11);
            binair = Convert.ToInt32("110", 2);
            uitkomst = getal * binair;

            Console.WriteLine($"{Convert.ToString(binair, 2)} * {Convert.ToString(getal, 2)} = {Convert.ToString(uitkomst, 2)}");

            getal = randomgen.Next(0, 11);
            binair = Convert.ToInt32("111", 2);
            uitkomst = getal * binair;

            Console.WriteLine($"{Convert.ToString(binair, 2)} * {Convert.ToString(getal, 2)} = {Convert.ToString(uitkomst, 2)}");

            getal = randomgen.Next(0, 11);
            binair = Convert.ToInt32("1000", 2);
            uitkomst = getal * binair;

            Console.WriteLine($"{Convert.ToString(binair, 2)} * {Convert.ToString(getal, 2)} = {Convert.ToString(uitkomst, 2)}");

            getal = randomgen.Next(0, 11);
            binair = Convert.ToInt32("1001", 2);
            uitkomst = getal * binair;

            Console.WriteLine($"{Convert.ToString(binair, 2)} * {Convert.ToString(getal, 2)} = {Convert.ToString(uitkomst, 2)}");

            getal = randomgen.Next(0, 11);
            binair = Convert.ToInt32("1010", 2);
            uitkomst = getal * binair;

            Console.WriteLine($"{Convert.ToString(binair, 2)} * {Convert.ToString(getal, 2)} = {Convert.ToString(uitkomst, 2)}");
        }
    }
}
