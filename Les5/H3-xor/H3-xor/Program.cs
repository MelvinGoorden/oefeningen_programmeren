﻿using System;

namespace H3_xor
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Geef een eerste getal?");
            int getal1 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Geef een tweede getal?");
            int getal2 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Geef nummer van de bit(te tellen vanaf de kleinste) ?");
            int bit = Convert.ToInt32(Console.ReadLine());

            getal1 = getal1 >> bit - 1;
            getal2 = getal2 >> bit - 1;

            int uitkomst = getal1 ^ getal2;
            bool x = Convert.ToBoolean(uitkomst & 1);

            Console.WriteLine($"Precies één bit met waarde 1: {x}");
        }
    }
}
