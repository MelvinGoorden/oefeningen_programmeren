﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrayViewer
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] array = { 15, 6, 9 };
            int[] array2 = { 0, 1, 2, 3, 4, 5, 6 };
            VisualiseArray(array);
            VisualiseArray(array2);
        }

        private static void VisualiseArray(int[] array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                Console.Write($"{array[i]}\t");
            }
            Console.WriteLine();
        }
    }
}
