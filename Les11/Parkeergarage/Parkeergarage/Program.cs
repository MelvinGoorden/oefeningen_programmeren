﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parkeergarage
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Geef aantal auto's in");
            int NumberOfCars = Convert.ToInt32(Console.ReadLine());

            double[] hoursPerCar = new double[NumberOfCars];

            for (int i = 0; i < NumberOfCars; i++)
            {
                Console.WriteLine($"Geef parkeertijd auto {i+1} in uren:");
                hoursPerCar[i] = Convert.ToDouble(Console.ReadLine());
            }
            double[] pricePerCar = Calculate(hoursPerCar);

            Console.WriteLine("Auto     Duur     Kost");
            for (int i = 0; i < NumberOfCars; i++)
            {
                Console.WriteLine($"{i}        {hoursPerCar[i]:f1}      {pricePerCar[i]:f2}");
            }
            Console.WriteLine($"Totaal   {hoursPerCar.Sum():f1}      {pricePerCar.Sum():f2}");


        }


        private static double CalculatePrice(double hours)
        {
            double price = 2;
            if ((hours > 3))
            {
                price += ((hours - 3) / 0.5) * 0.5;
            }
            if (price > 10)
            {
                price = 10;
            }
            
            return price;
        }

        static double[] Calculate(double[] hoursPerCar)
        {
            double[] Prices = new double[hoursPerCar.Length];
            for (int i = 0; i < hoursPerCar.Length; i++)
            {
                Prices[i] = CalculatePrice(hoursPerCar[i]);
            }

            return Prices;
        }
    }
}
