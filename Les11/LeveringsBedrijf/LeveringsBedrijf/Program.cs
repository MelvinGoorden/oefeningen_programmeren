﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeveringsBedrijf
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] postalCode = new int[] {2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009};
            int[] pricePerKg = new int[] {2,4,6,8,10,12,14,16,18,20};

            Console.WriteLine("Geef gewicht pakket:");
            int weight = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Naar welke postcode wenst u dit pakket te versturen?");
            int destination = Convert.ToInt32(Console.ReadLine());

            for (int i = 0; i < postalCode.Length; i++)
            {
                if (postalCode[i] == destination)
                {
                    Console.WriteLine($"Dit zal {weight * pricePerKg[i]} euro kosten.");
                }
            }

        }
    }
}
