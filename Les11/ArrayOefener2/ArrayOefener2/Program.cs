﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrayOefener2
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] A = new int[5];
            int[] B = new int[5];
            int[] C = new int[5];

            Console.WriteLine("Voer 5 gehele getallen in:");
            for (int i = 0; i < A.Length; i++)
            {
                A[i] = Convert.ToInt32(Console.ReadLine());
            }

            Console.WriteLine("Voer 5 gehele getallen in:");
            for (int i = 0; i < B.Length; i++)
            {
                B[i] = Convert.ToInt32(Console.ReadLine());
            }

            for (int i = 0; i < C.Length; i++)
            {
                C[i] = A[i] + B[i];
            }

            Console.WriteLine("Array C bevat:");
            foreach (var item in C)
            {
                Console.Write($"{item}, ");
            }
        }
    }
}
