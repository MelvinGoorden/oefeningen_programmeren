﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrayOefener1
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] numbers = new int[10];

            Console.WriteLine("Voer 10 gehele getallen in:");

            for (int i = 0; i < numbers.Length; i++)
            {
                numbers[i] = Convert.ToInt32(Console.ReadLine());
            }

            Console.WriteLine("*******");
            Console.WriteLine($"Som is {numbers.Sum()}, Gemiddelde is {numbers.Average()}, Grootste getal is {numbers.Max()}");
            Console.WriteLine("Geef een minimum getal in?");
            int minNumber = Convert.ToInt32(Console.ReadLine());
            foreach (var item in numbers)
            {
                if(minNumber <= item)
                {
                    Console.Write($"{item}, ");
                }
            }
        }
    }
}
