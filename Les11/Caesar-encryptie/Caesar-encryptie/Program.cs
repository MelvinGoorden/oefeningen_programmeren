﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Caesar_encryptie
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("geef de input in");
            string input = Console.ReadLine();
            input = input.ToUpper();
            char[] charArray = input.ToCharArray();
            char[] encryptedArray = Encrypt(charArray);
            Console.WriteLine(new string(encryptedArray));
            char[] decryptedArray = Decrypt(charArray);
            Console.WriteLine(new string(decryptedArray));

        }

        private static char[] Encrypt(char[] array)
        {
            char[] key = new char[] {'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'A', 'B', 'C' };
            char[] alphabet = new char[] { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
            for (int i = 0; i < array.Length; i++)
            {
                for (int y = 0; y < alphabet.Length; y++)
                {
                    if (array[i] == alphabet[y])
                    {
                        array[i] = key[y];
                        break;
                    }
                }
            }
            return array;
        }

        private static char[] Decrypt(char[] array)
        {
            char[] key = new char[] { 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'A', 'B', 'C' };
            char[] alphabet = new char[] { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
            for (int i = 0; i < array.Length; i++)
            {
                for (int y = 0; y < alphabet.Length; y++)
                {
                    if (array[i] == key[y])
                    {
                        array[i] = alphabet[y];
                        break;
                    }
                }
            }
            return array;
        }
    }
}
