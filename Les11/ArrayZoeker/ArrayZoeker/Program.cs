﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrayZoeker
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] numbers = new int[10];

            Console.WriteLine("Voer 10 gehele getallen in:");

            for (int i = 0; i < numbers.Length; i++)
            {
                numbers[i] = Convert.ToInt32(Console.ReadLine());
            }
            Console.WriteLine("***********");
            Console.WriteLine("welk getal moet verwijdert worden?");
            int removeNumber = Convert.ToInt32(Console.ReadLine());
            int teller = 0;
            for (int i = 0; i < numbers.Length; i++)
            {
                if (removeNumber == numbers[i])
                {
                    numbers[teller] = numbers[i];
                    teller--;
                }
                else
                {
                    numbers[teller] = numbers[i];
                }
                teller++;
            }
            while(teller != (numbers.Length))
            {
                numbers[teller] = -1;
                teller++;
            }

            Console.Write("resultaat is:");
            foreach (var item in numbers)
            {
                Console.Write($"{item}, ");
            }
        }
    }
}
