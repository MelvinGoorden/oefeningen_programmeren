﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VraagArray
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] vragen = new string[] { "Hoe oud ben je ?", "Wat is je postcode ?", "Hoeveel broers heb je ?", "Hoeveel zussen heb je ?", "Wanneer ben je geboren?" };
            int[] antwoorden = new int[vragen.Length];

            for (int i = 0; i < vragen.Length; i++)
            {
                Console.WriteLine(vragen[i]);
                antwoorden[i] = Convert.ToInt32(Console.ReadLine());
            }

            Console.WriteLine("Je antwoorden:");

            for (int i = 0; i < vragen.Length; i++)
            {
                Console.WriteLine($"{vragen[i]} {antwoorden[i]}");
            }
        }
    }
}
