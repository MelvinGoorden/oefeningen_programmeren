﻿using System;

namespace H2_weerstandberekenaar_deel2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Geef de waarde (uitgedrukt in een getal van 0 tot 9) van de eerste ring: ");
            int ring1 = Convert.ToInt32(Console.ReadLine());
            Console.Write("Geef de waarde (uitgedrukt in een getal van 0 tot 9) van de tweede ring: ");
            int ring2 = Convert.ToInt32(Console.ReadLine());
            Console.Write("Geef de waarde (uitgedrukt in een getal van -2 tot 7) van de derde ring (exponent): ");
            int ring3 = Convert.ToInt32(Console.ReadLine());

            double uitkomst = ((ring1 * 10) + ring2) * Math.Pow(10, ring3);

            Console.WriteLine("╔════════╦════════╦════════╦═══════════════╗");
            Console.WriteLine("║ ring 1 ║ ring2  ║ ring3  ║ totaal (Ohm)  ║");
            Console.WriteLine("╟────────╫────────╫────────╫───────────────╢");
            Console.WriteLine($"║ {ring1}      ║ {ring2}      ║ {ring3}      ║ {uitkomst} Ohm");
            Console.WriteLine("╚════════╩════════╩════════╩═══════════════╝");
        }
    }
}
