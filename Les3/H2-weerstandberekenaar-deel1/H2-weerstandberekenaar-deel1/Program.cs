﻿using System;

namespace H2_weerstandberekenaar_deel1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Geef de waarde (uitgedrukt in een getal van 0 tot 9) van de eerste ring: ");
            int ring1 = Convert.ToInt32(Console.ReadLine());
            Console.Write("Geef de waarde (uitgedrukt in een getal van 0 tot 9) van de tweede ring: ");
            int ring2 = Convert.ToInt32(Console.ReadLine());
            Console.Write("Geef de waarde (uitgedrukt in een getal van -2 tot 7) van de derde ring (exponent): ");
            int ring3 = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine($"Resultaat is {((ring1 * 10) + ring2) * Math.Pow(10, ring3)} Ohm, ofwel {(ring1 * 10) + ring2}x{Math.Pow(10, ring3)}.");
        }
    }
}
