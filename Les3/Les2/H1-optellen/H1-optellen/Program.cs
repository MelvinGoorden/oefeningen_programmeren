﻿using System;

namespace H1_optellen
{
    class Program
    {
        static void Main(string[] args)
        {
            double getal1, getal2, uitkomst;

            Console.WriteLine("Geef het eerst getal:");
            getal1 = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("Geef het tweede getal:");
            getal2 = Convert.ToDouble(Console.ReadLine());

            uitkomst = getal1 + getal2;
            Console.WriteLine($"De som is : {uitkomst}");
        }
    }
}
