﻿using System;

namespace H1_ruimte
{
    class Program
    {
        static void Main(string[] args)
        {
            int massa = 69;
            double gewicht;

            gewicht = massa * 0.38;
            Console.WriteLine($"Op Mercurius heb je een schijnbaar gewicht van {gewicht}kg.");

            gewicht = massa * 0.91;
            Console.WriteLine($"Op Venus heb je een schijnbaar gewicht van {gewicht}kg.");

            gewicht = massa * 1.00;
            Console.WriteLine($"Op Aarde heb je een schijnbaar gewicht van {gewicht}kg.");

            gewicht = massa * 0.38;
            Console.WriteLine($"Op Mars heb je een schijnbaar gewicht van {gewicht}kg.");

            gewicht = massa * 2.34;
            Console.WriteLine($"Op Jupiter heb je een schijnbaar gewicht van {gewicht}kg.");

            gewicht = massa * 1.06;
            Console.WriteLine($"Op Saturnus heb je een schijnbaar gewicht van {gewicht}kg.");

            gewicht = massa * 0.92;
            Console.WriteLine($"Op Uranus heb je een schijnbaar gewicht van {gewicht}kg.");

            gewicht = massa * 1.19;
            Console.WriteLine($"Op Neptunus heb je een schijnbaar gewicht van {gewicht}kg.");

            gewicht = massa * 0.06;
            Console.WriteLine($"Op Pluto heb je een schijnbaar gewicht van {gewicht}kg.");
        }
    }
}
