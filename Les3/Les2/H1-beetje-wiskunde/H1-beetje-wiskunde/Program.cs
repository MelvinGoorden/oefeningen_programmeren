﻿using System;

namespace H1_beetje_wiskunde
{
    class Program
    {
        static void Main(string[] args)
        {
            int getal1 = -1 + 4 * 6;
            int getal2 = (35 + 5) % 7;
            int getal3 = 14 + -4 * 6 / 11;
            int getal4 = 2 + 15 / 6 * 1 - 7 % 2;

            Console.WriteLine(getal1);
            Console.WriteLine(getal2);
            Console.WriteLine(getal3);
            Console.WriteLine(getal4);
            Console.WriteLine();

            float getal5 = -1 + 4 * 6;
            float getal6 = (35 + 5f) % 7;
            float getal7 = 14 + -4f * 6 / 11;
            float getal8 = 2 + 15f / 6 * 1 - 7f % 2;

            Console.WriteLine(getal5);
            Console.WriteLine(getal6);
            Console.WriteLine(getal7);
            Console.WriteLine(getal8);
        }
    }
}
