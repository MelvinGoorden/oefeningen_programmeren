﻿using System;

namespace H1_maaltafels
{
    class Program
    {
        static void Main(string[] args)
        {
            int teller = 1;
            int uitkomst;

            while(true)
            {
                uitkomst = teller * 411;
                Console.WriteLine($"{teller} * 411 is {uitkomst}");
                Console.ReadKey();
                Console.Clear();
                teller++;
            }
        }
    }
}
