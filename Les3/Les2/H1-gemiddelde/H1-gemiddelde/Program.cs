﻿using System;

namespace H1_gemiddelde
{
    class Program
    {
        static void Main(string[] args)
        {
            float getal1 = 18f;
            int getal2 = 11;
            int getal3 = 8;

            float uitkomst = (getal1 + getal2 + getal3) / 3;
            Console.WriteLine(uitkomst);
        }
    }
}
