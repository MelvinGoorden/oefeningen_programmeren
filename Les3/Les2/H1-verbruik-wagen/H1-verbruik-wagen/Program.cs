﻿using System;

namespace H1_verbruik_wagen
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Geef het aantal liters in de tank voor de rit: ");
            double litertank1 = Convert.ToDouble(Console.ReadLine());

            Console.Write("Geef het aantal liters in de tank na de rit: ");
            double litertank2 = Convert.ToDouble(Console.ReadLine());

            Console.Write("Geef het aantal kilometers op de teller voor de rit: ");
            double kilometerStand1 = Convert.ToDouble(Console.ReadLine());

            Console.Write("Geef het aantal kilometers op de teller na de rit: ");
            double kilometerStand2 = Convert.ToDouble(Console.ReadLine());

            double verbruik = 100 * ((litertank1 - litertank2) / (kilometerStand2 - kilometerStand1));
            Console.WriteLine($"het verbruik van de autos is: {verbruik}");
        }
    }
}
