﻿using System;

namespace H1_variabelen_hoofdletters
{
    class Program
    {
        static void Main(string[] args)
        {
            string text;

            Console.WriteLine("Wat wilt u herhaald zien worden in hoofdletters?");
            text = Console.ReadLine().ToUpper();

            Console.WriteLine($"Uw invoer in hoofdletters is: {text}");

        }
    }
}
