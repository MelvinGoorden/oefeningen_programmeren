﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace H5_priem_checker
{
    class Program
    {
        static void Main(string[] args)
        {
            int getal, teller = 0;

            Console.Write("Geef een getal: ");
            getal = Convert.ToInt32(Console.ReadLine());

            for(int i = 1; i <= getal; i++)
            {
                if(((getal % i) == 0))
                {
                    teller++;
                }
            }

            if(teller == 2)
            {
                Console.WriteLine("Het getal is een priemgetal!");
            }
            else
            {
                Console.WriteLine("Het getal is geen priemgetal!");
            }
        }
    }
}
