﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace H5_boekhouder
{
    class Program
    {
        static void Main(string[] args)
        {
            int inputNumber = 0, total = 0, sumPositives = 0, sumNegatives = 0, average = 0, averageCounter = 0;

            while(true)
            {
                Console.Write("Geef een getal: ");
                inputNumber = Convert.ToInt32(Console.ReadLine());

                total += inputNumber;
                Console.WriteLine($"De balans is {total}");

                if(inputNumber > 0)
                {
                    sumPositives += inputNumber;
                }
                else
                {
                    sumNegatives += inputNumber;
                }
                Console.WriteLine($"De som van de positieve getallen is {sumPositives}");
                Console.WriteLine($"De som van de negatieve getallen is {sumNegatives}");

                averageCounter++;
                average = total / averageCounter;
                Console.WriteLine($"Het gemiddelde is {average}");
            }
        }
    }
}
