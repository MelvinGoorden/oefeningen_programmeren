﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace H5_priem_generator
{
    class Program
    {
        static void Main(string[] args)
        {
            int inputNumber, counter = 0;

            Console.Write("geef een getal: ");
            inputNumber = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Alle priemgetallen kleiner dan of gelijk aan 11 zijn:");

            for(int i = inputNumber; i > 1; i--)
            {
                counter = 0;

                for (int y = 1; y <= i; y++)
                {
                    if (((i % y) == 0))
                    {
                        counter++;
                    }
                }

                if (counter == 2)
                {
                    Console.WriteLine(i);
                }
            }
        }
    }
}
