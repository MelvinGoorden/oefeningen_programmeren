﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace H5_schaarsteenpaper
{
    class Program
    {
        enum game {schaar = 1, steen, papier}
        static void Main(string[] args)
        {
            Random randomNumber = new Random();
            int winCounterPc = 0, winCounterUser = 0;
            game inputUser,inputPc;

            while(!((winCounterPc == 10) || (winCounterUser == 10)))
            {
                Console.WriteLine("Maak een keuze:");
                Console.WriteLine("1 voor schaar");
                Console.WriteLine("2 voor steen");
                Console.WriteLine("3 voor papier");

                inputUser = (game) Convert.ToInt32(Console.ReadLine());
                inputPc = (game) randomNumber.Next(1,4);

                Console.WriteLine($"De computer kiest {inputPc}!");

                if(((inputUser == game.papier) && (inputPc == game.schaar)))
                {
                    Console.WriteLine("De computer wint deze ronde!");
                    winCounterPc++;
                }
                else if (((inputUser == game.papier) && (inputPc == game.steen)))
                {
                    Console.WriteLine("Jij wint deze ronde!");
                    winCounterUser++;
                }
                else if (((inputUser == game.schaar) && (inputPc == game.papier)))
                {
                    Console.WriteLine("Jij wint deze ronde!");
                    winCounterUser++;
                }
                else if (((inputUser == game.schaar) && (inputPc == game.steen)))
                {
                    Console.WriteLine("De computer wint deze ronde!");
                    winCounterPc++;
                }
                else if (((inputUser == game.steen) && (inputPc == game.papier)))
                {
                    Console.WriteLine("De computer wint deze ronde!");
                    winCounterPc++;
                }
                else if (((inputUser == game.steen) && (inputPc == game.schaar)))
                {
                    Console.WriteLine("Jij wint deze ronde!");
                    winCounterUser++;
                }
                else
                {
                    Console.WriteLine("Niemand wint deze ronde!");
                }

                Console.WriteLine($"Jij hebt {winCounterUser} punt, de computer heeft {winCounterPc} punten.");
            }
            
        }
    }
}
