﻿using System;

namespace H4_schoenverkoper
{
    class Program
    {
        static void Main(string[] args)
        {
            int aantal, bedrag, kortingaantal;
            const int prijs = 20;
            const int kortingprijs = 10;

            Console.WriteLine("Vanaf welk aantal geldt de korting?");
            kortingaantal = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Hoeveel paar schoenen wil je kopen?");
            aantal = Convert.ToInt32(Console.ReadLine());

            if(aantal < kortingaantal)
            {
                bedrag = prijs * aantal;
                Console.WriteLine($"Je moet {bedrag} euro betalen.");
            }
            else
            {
                bedrag = kortingprijs * aantal;
                Console.WriteLine($"Je moet {bedrag} euro betalen.");
            }
        }
    }
}
