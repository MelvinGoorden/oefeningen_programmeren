﻿using System;

namespace H4_orakeltje
{
    enum sexes { Male, Female}
    class Program
    {
        static void Main(string[] args)
        {
            sexes geslacht;
            Random randomgen = new Random();
            int max;

            Console.WriteLine("Wat is je geslacht? (m/v)");
            char x = Convert.ToChar(Console.ReadLine());

            if(x == 'm')
            {
                geslacht = sexes.Male; 
            }
            else
            {
                geslacht = sexes.Female;
            }

            Console.WriteLine("Hoe oud ben je?");
            int leeftijd = Convert.ToInt32(Console.ReadLine());

            switch (geslacht)
            {
                case sexes.Male:
                    max = 120 - leeftijd;
                    Console.WriteLine($"Je zal nog {randomgen.Next(5, max)} jaar leven.");
                    break;
                case sexes.Female:
                    max = 120 - leeftijd;
                    Console.WriteLine($"Je zal nog {randomgen.Next(5, max)} jaar leven.");
                    break;
                default:
                    Console.WriteLine("Default case");
                    break;
            }    
        }
    }
}
