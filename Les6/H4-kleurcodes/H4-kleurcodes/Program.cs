﻿using System;

namespace H4_kleurcodes
{
    class Program
    {
        static void Main(string[] args)
        {
            string swaarde = null;
            double dwaarde;

            Console.WriteLine("Wat is de kleur van de eerste ring ? ");
            string ring1 = Console.ReadLine();

            switch (ring1)
            {
                case "zwart":
                    swaarde = "0";
                    break;
                case "bruin":
                    swaarde = "1";
                    break;
                case "rood":
                    swaarde = "2";
                    break;
                case "oranje":
                    swaarde = "3";
                    break;
                case "geel":
                    swaarde = "4";
                    break;
                case "groen":
                    swaarde = "5";
                    break;
                case "blauw":
                    swaarde = "6";
                    break;
                case "paars":
                    swaarde = "7";
                    break;
                case "grijs":
                    swaarde = "8";
                    break;
                case "wit":
                    swaarde = "9";
                    break;
                default:
                    Console.WriteLine("verkeerde kleur");
                    break;
            }

            Console.WriteLine("Wat is de kleur van de tweede ring ? ");
            string ring2 = Console.ReadLine();

            switch (ring2)
            {
                case "zwart":
                    swaarde += "0";
                    break;
                case "bruin":
                    swaarde += "1";
                    break;
                case "rood":
                    swaarde += "2";
                    break;
                case "oranje":
                    swaarde += "3";
                    break;
                case "geel":
                    swaarde += "4";
                    break;
                case "groen":
                    swaarde += "5";
                    break;
                case "blauw":
                    swaarde += "6";
                    break;
                case "paars":
                    swaarde += "7";
                    break;
                case "grijs":
                    swaarde += "8";
                    break;
                case "wit":
                    swaarde += "9";
                    break;
                default:
                    Console.WriteLine("verkeerde kleur");
                    break;
            }

            dwaarde = Convert.ToDouble(swaarde);

            Console.WriteLine("Wat is de kleur van de derde ring ? ");
            string ring3 = Console.ReadLine();

            switch (ring3)
            {
                case "zwart":
                    dwaarde *= 1; 
                    break;
                case "bruin":
                    dwaarde *= 10;
                    break;
                case "rood":
                    dwaarde *= 100;
                    break;
                case "oranje":
                    dwaarde *= 1000;
                    break;
                case "geel":
                    dwaarde *= 10000;
                    break;
                case "groen":
                    dwaarde *= 100000;
                    break;
                case "blauw":
                    dwaarde *= 1000000;
                    break;
                case "paars":
                    dwaarde *= 10000000;
                    break;
                case "grijs":
                    dwaarde *= 100000000;
                    break;
                case "wit":
                    dwaarde *= 1000000000;
                    break;
                default:
                    Console.WriteLine("verkeerde kleur");
                    break;
            }

            Console.WriteLine($"Deze weerstand heeft waarde van {dwaarde} Ohm");
        }
    }
}
