﻿using System;

namespace H4_Ohm_berekenaar
{
    class Program
    {
        static void Main(string[] args)
        {
            string keuze;
            double R, U, I;

            Console.WriteLine("Wat wil je berekenen? spanning, weerstand of stroomsterkte?");
            keuze = Console.ReadLine();

            if(keuze == "stroomsterkte")
            {
                Console.WriteLine("Wat is de spanning?");
                U = Convert.ToDouble(Console.ReadLine());
                Console.WriteLine("Wat is de Weerstand?");
                R = Convert.ToDouble(Console.ReadLine());

                I = U / R;

                Console.WriteLine($"De spanning bedraagt {I} A.");

            }
            else if(keuze == "spanning")
            {
                Console.WriteLine("Wat is de stroomsterkte?");
                I = Convert.ToDouble(Console.ReadLine());
                Console.WriteLine("Wat is de Weerstand?");
                R = Convert.ToDouble(Console.ReadLine());

                U = I * R;

                Console.WriteLine($"De spanning bedraagt {U} V.");
            }
            else if(keuze == "weerstand")
            {
                Console.WriteLine("Wat is de spanning?");
                I = Convert.ToDouble(Console.ReadLine());
                Console.WriteLine("Wat is de spanning?");
                U = Convert.ToDouble(Console.ReadLine());

                R = U / I;

                Console.WriteLine($"De spanning bedraagt {R} ohm.");
            }
            else
            {
                Console.WriteLine("verkeerde keuze probeer opnieuw");
            }
        }
    }
}
