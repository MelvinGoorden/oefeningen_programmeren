﻿using System;

namespace H4_Schrikkeljaar
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("geef uw jaar in : ");
            int jaar = Convert.ToInt32(Console.ReadLine());

            if((jaar % 4) == 0)
            {
                if ((jaar % 100) == 0)
                {
                    if ((jaar % 400) == 0)
                    {
                        Console.WriteLine("schrikkeljaar");
                    }
                    else
                    {
                        Console.WriteLine("geen schrikkeljaar");
                    }
                }
                else
                {
                    Console.WriteLine("geen schrikkeljaar");
                }
            }
            else
            {
                Console.WriteLine("geen schrikkeljaar");
            }

        }
    }
}
