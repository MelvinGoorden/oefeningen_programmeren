﻿using System;

namespace BMI_met_if
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hoe veel weeg je in kg?");
            double gewicht = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Hoe groot ben je in m?");
            double grootte = Convert.ToDouble(Console.ReadLine());

            double BMI = gewicht / Math.Pow(grootte, 2);
            Console.WriteLine($"Je BMI bedraagt {Math.Round(BMI, 2)}.");

            if (BMI < 18.5)
            {
                Console.ForegroundColor =ConsoleColor.Red;

                Console.WriteLine("ondergewicht");
            }
            else if ((BMI >= 18.5) && (BMI < 25))
            {
                Console.ForegroundColor = ConsoleColor.Green;

                Console.WriteLine("normaal gewicht");
            }
            else if ((BMI >= 25) && (BMI < 30))
            {
                Console.ForegroundColor = ConsoleColor.Yellow;

                Console.WriteLine("overgewicht");
            }
            else if ((BMI >= 30) && (BMI < 40))
            {
                Console.ForegroundColor = ConsoleColor.Red;

                Console.WriteLine("zwaarlijvig");
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Magenta;

                Console.WriteLine("ernstige obesitas");
            }
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}
