﻿using System;

namespace H0_gekleurde_rommelzin
{
    class Program
    {
        static void Main(string[] args)
        {
            string kleur, eten, auto, film, boek;

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Wat is je favoriete kleur?");
            Console.ResetColor();
            kleur = Console.ReadLine();

            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.WriteLine("Wat is je favoriete eten?");
            Console.ResetColor();
            eten = Console.ReadLine();

            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Wat is je favoriete auto?");
            Console.ResetColor();
            auto = Console.ReadLine();

            Console.ForegroundColor = ConsoleColor.DarkBlue;
            Console.WriteLine("Wat is je favoriete film?");
            Console.ResetColor();
            film = Console.ReadLine();

            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Wat is je favoriete boek?");
            Console.ResetColor();
            boek = Console.ReadLine();

            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine();
            Console.WriteLine($"Je favoriete kleur is {eten}. Je eet graag {kleur}. Je lievelingsfilm is {boek} en je favoriete boek is {film}.");
        }
    }
}
