﻿using System;

namespace H0_eerste_programma
{
    class Program
    {
        static void Main(string[] args)
        {
            string naam;

            Console.WriteLine("dit is mijn eerste c#-programma");
            Console.WriteLine("*******************************");
            Console.WriteLine();
            Console.Write("Geef uw naam: ");
            naam = Console.ReadLine();
            Console.WriteLine($"Hallo, {naam}.");

        }

    }
}
