﻿using System;

namespace H0_eerste_programma_pro
{
    class Program
    {
        static void Main(string[] args)
        {
            string achternaam, voornaam;

            Console.WriteLine("dit is mijn eerste c#-programma");
            Console.WriteLine("*******************************");
            Console.WriteLine();
            Console.Write("Geef uw voornaam: ");
            voornaam = Console.ReadLine();
            Console.Write("Geef uw achternaam: ");
            achternaam = Console.ReadLine();

            Console.WriteLine($"dus uw naam is {achternaam} {voornaam}.");
            Console.WriteLine($"Of: {voornaam} {achternaam}");
        }
    }
}
