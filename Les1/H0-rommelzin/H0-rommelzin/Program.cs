﻿using System;

namespace H0_rommelzin
{
    class Program
    {
        static void Main(string[] args)
        {
            string kleur, eten, auto, film, boek;

            Console.WriteLine("Wat is je favoriete kleur?");
            kleur = Console.ReadLine();

            Console.WriteLine("Wat is je favoriete eten?");
            eten = Console.ReadLine();

            Console.WriteLine("Wat is je favoriete auto?");
            auto = Console.ReadLine();

            Console.WriteLine("Wat is je favoriete film?");
            film = Console.ReadLine();

            Console.WriteLine("Wat is je favoriete boek?");
            boek = Console.ReadLine();

            Console.WriteLine();
            Console.WriteLine($"Je favoriete kleur is {eten}. Je eet graag {kleur}. Je lievelingsfilm is {boek} en je favoriete boek is {film}.");

        }
    }
}
