﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace H6_voorstellen_plus
{
    class Program
    {
        static void Main(string[] args)
        {
            MyIntro("Tim Dams", 18, "Lambrisseringsstraat 666");
        }

        private static void MyIntro(string name, int age, string address)
        {
            Console.WriteLine($"Ik ben {name}, ik ben {age} jaar oud en woon in de {address}");
        }
    }
}
