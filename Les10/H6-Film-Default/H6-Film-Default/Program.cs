﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace H6_Film_Default
{
    class Program
    {
        static void Main(string[] args)
        {
            FilmRuntime("The Matrix", 120, FilmGenre.Action);
            FilmRuntime(filmName:"Crouching Tiger, Hidden Dragon", filmType:FilmGenre.Uncategorized);
        }

        private static void FilmRuntime(string filmName, int filmLength = 90, FilmGenre filmType = FilmGenre.Uncategorized)
        {
            Console.WriteLine($"{filmName}({filmLength} minuten, {filmType})");
        }
    }
}
