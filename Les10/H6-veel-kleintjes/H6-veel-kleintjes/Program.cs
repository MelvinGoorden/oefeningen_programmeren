﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace H6_veel_kleintjes
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Wat moet ik kwadrateren?");
            double inputkwadraat = Convert.ToDouble(Console.ReadLine());
            Square(inputkwadraat);

            Console.WriteLine("Wat is de diameter?");
            double inputDiameter = Convert.ToDouble(Console.ReadLine());
            Radius(inputDiameter);

            Console.WriteLine("Wat is de diameter?");
            inputDiameter = Convert.ToDouble(Console.ReadLine());
            Circumference(inputDiameter);
            Surface(inputDiameter);

            Console.WriteLine("Welke twee getallen wil je vergelijken?");
            int number1 = Convert.ToInt32(Console.ReadLine());
            int number2 = Convert.ToInt32(Console.ReadLine());
            Largest(number1, number2);

            Console.WriteLine("Geef een getal en ik zeg of het even is:");
            int NumberEven = Convert.ToInt32(Console.ReadLine());
            bool even = IsEven(NumberEven);

            if (even)
            {
                Console.WriteLine("Het getal is even.");
            }
            else
            {
                Console.WriteLine("Het getal is oneven.");
            }

            Console.WriteLine("Geef een getal en ik zoek de oneven getallen:");
            int NumberUneven = Convert.ToInt32(Console.ReadLine());
            ShowOdd(NumberUneven);

        }

        private static void ShowOdd(int number)
        {
            for (int i = 0; i < number; i++)
            {
                if((i%2) != 0)
                {
                    Console.WriteLine(i);
                }
            }
        }

        private static bool IsEven(int number)
        {
            if((number%2) == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private static void Largest(int number1, int number2)
        {
            if(number1 < number2)
            {
                Console.WriteLine($"{number2} is het grootste getal");
            }
            else
            {
                Console.WriteLine($"{number1} is het grootste getal");
            }
        }

        private static void Surface(double diameter)
        {
            Console.WriteLine($"De oppervlakte is {Math.Pow((diameter / 2),2) * Math.PI}");
        }

        private static void Circumference(double diameter)
        {
            Console.WriteLine($"De omtrek is {2*Math.PI*(diameter/2)}");
        }

        private static void Radius(double diameter)
        {
            Console.WriteLine(diameter / 2);
        }

        private static void Square(double Number)
        {
            Console.WriteLine(Math.Pow(Number,2));
        }
    }
}
