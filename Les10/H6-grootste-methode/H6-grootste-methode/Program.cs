﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace H6_grootste_methode
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Geef 3 ints.");
            int number1 = Convert.ToInt32(Console.ReadLine());
            int number2 = Convert.ToInt32(Console.ReadLine());
            int number3 = Convert.ToInt32(Console.ReadLine());

            int largestNumber = Largest(number1, number2, number3);

            Console.WriteLine($"Het grootste getal is {largestNumber}.");
        }

        private static int Largest(int number1, int number2, int number3)
        {
            if((number1 > number2) && (number1 > number3))
            {
                return number1;
            }
            else if((number2 > number1) && (number2 > number3))
            {
                return number2;
            }
            else
            {
                return number3;
            }
        }
    }
}
