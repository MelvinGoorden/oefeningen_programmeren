﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace H6_pasw_gen
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hoe veel karakters moet je wachtwoord bevatten?");
            int input = Convert.ToInt32(Console.ReadLine());

            string password = passwordGenerator(input);

            Console.WriteLine(password);
        }

        private static string passwordGenerator(int length)
        {
            Random randomNumber = new Random();
            string password = null;

            for (int i = 0; i < length; i++)
            {
                password += (char)randomNumber.Next(64,121);
            }
            return password;
        }
    }
}
